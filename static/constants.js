export const routePath = {
	favorites: 'favorites',
	pokemonDetails: 'pokemon-details',
	pokemonList: 'pokemon-list',
	userTeamDetails: 'user-team-details',
	index: '/'
}