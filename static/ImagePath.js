import pokeLogo from '../assets/poke-logo.png';
import pokeBall from '../assets/pokeball.png';
import pokemonBallColored from '../assets/pokemon-ball.png';

export const ImagePath = {
	pokeLogo: {
		src: pokeLogo,
		alt: 'poke logo'
	},
	pokeBall: {
		src: pokeBall,
		alt: 'poke ball'
	},
	pokemonBallColored: {
		src: pokemonBallColored,
		alt: 'pokemon ball colored'
	}
}