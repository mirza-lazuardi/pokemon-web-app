export const state = () => ({
	loading: false,
	pokemonList: [],
	selectedPokemon: {}
});

export const getters = {
	getPokemonList(state) {
		return state.pokemonList;
	},
	getSelectedPokemon(state) {
		return state.selectedPokemon;
	}
}

export const actions = {
	async fetchPokemonList({ commit }, page) {
		const offset = (page - 1) * 10;
		const res = await this.$axios.$get(`https://pokeapi.co/api/v2/pokemon?limit=10&offset=${offset}`);
		commit('setPokemonList', res.results);
	},
	async fetchPokemonDetail({commit}, id) {
		const res = await this.$axios.$get(`https://pokeapi.co/api/v2/pokemon/${id}`);
		commit('setSelectedPokemon', res);
	}
}

export const mutations = {
	setLoading(loading){
		state.loading = loading;
	},
	setPokemonList(state, pokemonList) {
		state.pokemonList = [];
		if (pokemonList) {
			pokemonList.forEach(each => {
				const id = each.url.substr(34, each.url.lastIndexOf('/'))
				const pokemon = {
					id: id.substr(0, id.length-1),
					name: each.name
				}
				state.pokemonList.push(pokemon);
			});
		}
	},
	addPokemon(state, pokemon){
		state.pokemonList.push(pokemon);
	},
	setSelectedPokemon(state, pokemon){
		const poke = {
			name: pokemon.name,
			id: pokemon.id
		}
		state.selectedPokemon = poke;
	}
}

