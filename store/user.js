export const state = () => ({
	loading: false,
	name: '',
	team: [],
	selectedTeam: null,
	favorites: []
});

export const getters = {
	getTeamList(state) {
		return state.team;
	},
	getFavoritesListPokemon(state) {
		return state.favorites;
	},
	getSelectedTeam(state) {
		return state.selectedTeam;
	},
	getPokemonListByTeam(state){
		if (state.selectedTeam) {
			return state.selectedTeam.pokemonList;
		}
		return [];
	},
	isLogin(state) {
		return state.name !== '';
	}
}

export const actions = {
	createUser({ commit }, user) {
		commit('setLoading', true);
		commit('setUser', user);
		commit('addTeam', user.team);
		commit('setLoading', true);
	},
	createTeam({commit}, team) {
		commit('setLoading', true);
		commit('addTeam', team);
		commit('setLoading', false);
	},
	selectTeam({commit}, team) {
		commit('setLoading', true);
		commit('setSelectedTeam', team);
		commit('setLoading', false);
	},
	editTeamName({commit}, name) {
		commit('setLoading', true);
		commit('editTeamName', name);
		commit('setLoading', false);
	},
	deleteTeam({commit}, index) {
		commit('setLoading', true);
		commit('deleteTeam', index);
		commit('setLoading', false);
	},
	deletePokemonFromTeam({commit}, pokemon) {
		commit('setLoading', true);
		commit('deletePokemonFromTeam', pokemon);
		commit('setLoading', false);
	},
	addPokemonToTeam({commit}, teamIndex, pokemon) {
		commit('setLoading', true);
		commit('addPokemonToTeam', teamIndex, pokemon);
		commit('setLoading', false);
	},
	addPokemonToFavorite({commit}, pokemon) {
		commit('setLoading', true);
		commit('addPokemonToFavorite', pokemon);
		commit('setLoading', false);
	},
	removePokemonFromFavorite({commit}, pokemon) {
		commit('setLoading', true);
		commit('removeFromFavorite', pokemon);
		commit('setLoading', false);
	},
	reset({commit}) {
		commit('setLoading', true);
		commit('reset');
		commit('setLoading', false);
	}
}

export const mutations = {
	setLoading(state, loading) {
		state.loading = loading;
	},
	setUser(state, user){
		state.name = user.name;
	},
	setSelectedTeam(state, team) {
		state.selectedTeam = team;
	},
	editTeamName(state, name) {
		state.selectedTeam.name = name;
	},
	addTeam(state, team) {
		state.team.push(team);
		state.selectedTeam = team;
	},
	addPokemonToTeam(state, pokemon) {
		state.selectedTeam.pokemonList.push(pokemon);
	},
	addPokemonToFavorite(state, pokemon) {
		state.favorites.push(pokemon);
	},
	deleteTeam(state, index) {
		state.team.splice(index, 1);
	},
	deletePokemonFromTeam(state, index) {
		state.selectedTeam.pokemonList.splice(index, 1);
	},
	removeFromFavorite(state, pokemon) {
		const index = state.favorites.indexOf(pokemon)
		state.favorites.splice(index, 1);
	},
	reset(state){
		state.name = '';
		state.team = [];
		state.favorites = [];
		state.selectedTeam = null;
	}
}

