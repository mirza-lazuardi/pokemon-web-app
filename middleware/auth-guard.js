export default function ({ store, route, redirect }) {
	if(route.path !== '/' && !store.getters['user/isLogin']) {
		return redirect('/');
	} 
}